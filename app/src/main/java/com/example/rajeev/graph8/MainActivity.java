package com.example.rajeev.graph8;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PaintView paintView = (PaintView)findViewById(R.id.paint_view);
        TextView tvCoordinates = (TextView)findViewById(R.id.tv_coordinates);
        paintView.setTextView(tvCoordinates);
        paintView.setOnTouchListener(paintView);

    }




}

