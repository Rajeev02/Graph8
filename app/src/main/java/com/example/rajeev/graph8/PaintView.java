package com.example.rajeev.graph8;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class PaintView extends View implements OnTouchListener{

	Paint mPaint;
	float mX;
	float mY;	
	TextView mTVCoordinates;
	
	public PaintView(Context context,AttributeSet attributeSet){
		super(context,attributeSet);

		mPaint = new Paint();
		mX = mY = -100;
		mTVCoordinates = null;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {		
		super.onDraw(canvas);
		mPaint.setColor(Color.BLUE);
		int radius = 30;
		canvas.drawCircle(mX, mY, radius, mPaint);
		invalidate();
	}
	
	public void setTextView(TextView tv){
		mTVCoordinates = tv;		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				mX = event.getX();
				mY = event.getY();
				if(mTVCoordinates!=null){
					mTVCoordinates.setText("X :" + mX + " , " + "Y :" + mY);
				}
		}
		return true;
	}
}